################################################################################
# Global Arguments
################################################################################
ARG DOCKER_TAG

################################################################################
# Host Image
################################################################################
FROM docker:${DOCKER_TAG}

# Setup host environment with common utilities.
RUN apk update && \
  apk add --no-cache \
    git \
    make
