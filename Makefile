################################################################################
# Settings
################################################################################

VENDOR_NAME=agogpixel
APP_NAME=dind-host
CR_HOST=registry.gitlab.com
GROUP_PATH=devops/docker

DOCKER_TAGS=19.03.1

################################################################################
# Parameters
################################################################################

CR_USER?=guest
CR_PASSWORD?=itsasecrettoeveryone

################################################################################
# Computed Values
################################################################################

GIT_SHORT_COMMIT_HASH=$(shell git diff-index --quiet HEAD && git rev-parse --short HEAD)

IMAGE_NAME=$(VENDOR_NAME)/$(APP_NAME)
CR_IMAGE_NAME=$(CR_HOST)/$(VENDOR_NAME)/$(GROUP_PATH)/$(APP_NAME)

IMAGE_LATEST_SOURCE=$(IMAGE_NAME):$(lastword $(DOCKER_TAGS))

ifeq ($(GIT_SHORT_COMMIT_HASH),)
	IMAGE_LATEST_SOURCE:=$(IMAGE_LATEST_SOURCE)-dirty
endif

################################################################################
# Rules
################################################################################

build: Dockerfile
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker build -t $(IMAGE_NAME):$(DOCKER_TAG) -t $(IMAGE_NAME):$(DOCKER_TAG)-$(GIT_SHORT_COMMIT_HASH) --build-arg DOCKER_TAG=$(DOCKER_TAG) .;)
else
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker build -t $(IMAGE_NAME):$(DOCKER_TAG)-dirty --build-arg DOCKER_TAG=$(DOCKER_TAG) .;)
endif
	docker tag $(IMAGE_LATEST_SOURCE) $(IMAGE_NAME):latest

test: build
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker run --rm -d $(IMAGE_NAME):$(DOCKER_TAG);)
else
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker run --rm -d $(IMAGE_NAME):$(DOCKER_TAG)-dirty;)
endif

release: login test tag
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker push $(CR_IMAGE_NAME):$(DOCKER_TAG); docker push $(CR_IMAGE_NAME):$(DOCKER_TAG)-$(GIT_SHORT_COMMIT_HASH);)
else
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker push $(CR_IMAGE_NAME):$(DOCKER_TAG)-dirty;)
endif
	docker push $(CR_IMAGE_NAME):latest

login:
	docker login -u $(CR_USER) -p $(CR_PASSWORD) $(CR_HOST)

tag:
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker tag $(IMAGE_NAME):$(DOCKER_TAG) $(CR_IMAGE_NAME):$(DOCKER_TAG);)
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker tag $(IMAGE_NAME):$(DOCKER_TAG)-$(GIT_SHORT_COMMIT_HASH) $(CR_IMAGE_NAME):$(DOCKER_TAG)-$(GIT_SHORT_COMMIT_HASH);)
else
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker tag $(IMAGE_NAME):$(DOCKER_TAG)-dirty $(CR_IMAGE_NAME):$(DOCKER_TAG)-dirty;)
endif
	docker tag $(IMAGE_NAME):latest $(CR_IMAGE_NAME):latest
