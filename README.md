# dind-host

Custom host image for use with [Docker in Docker](https://hub.docker.com/_/docker) use cases on [GitLab CI/CD](https://docs.gitlab.com/ce/ci/).

## Usage

Known working `image` & `service` value combinations (declared in `.gitlab-ci.yml`) for GitLab CI/CD:

| image          | service             |
|:--------------:|:-------------------:|
| docker:19.03.1 | docker:19.03.5-dind |

## Development

- Project utilizes [GitHub flow](https://guides.github.com/introduction/flow/).
- Images built and released to project's [container registry](https://gitlab.com/agogpixel/devops/docker/dind-host/container_registry).
